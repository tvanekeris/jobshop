# Running the code and further development

All the commands below have to be executed in the `src` directory.

## Install dependencies

```
pip install -r requirements.txt
```


## Regenerate paper results

To generate the results from the paper, look into the file

```
workflow_paper.py
```

and either run the whole file with

```
python workflow_paper.py
```

or comment out the parts of the code that you do not want to test.


## Define your own studies

Adapt the study paramters in `workflow.py` and run
```
python workflow.py
```


## Tensorboard integration

To start tensorboard, run
```
tensorboard --logdir ./tensorboard
```
in the main directory


## Testing

For running the tests of the framework, go into the `src` directory and execute:

```
python -m unittest
```