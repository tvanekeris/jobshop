# Deep Reinforcement Learning (DRL) for Jobshop Scheduling Problems (JSP) - an evaluation framework

This evaluation framework was written to study the use of Deep
Reinforcement Learning (DRL) for scheduling (or production planning)
purposes, specifically for solving the Jobshop Scheduling Problem (JSP).

The framework allows generating JSP data, training DRL agents that can
solve the JSPs and test the agents against an optimal solver (CP-SAT) and
simpler scheduling heuristics.

The framework was written by Tilo van Ekeris (tilo@vanekeris.de) while
working on the scientific publication
"Discovering Heuristics And Metaheuristics For Job Shop Scheduling
From Scratch Via Deep Reinforcement Learning" (van Ekeris, Meyes,
Meisen; 2021)
which has been accepted for publication at the
[2nd Conference on Production Systems and Logistics
(CPSL 2021)](https://cpsl-conference.com/).
A direct link to the publication will be provided here upon availability.

Please refer to `DEVELOPMENT.md` if you would like to regenerate the
publication results or work with the code.


## Acknowledgements

This repository was created within the Public Research Project
[AlphaMES](https://www.tmdt.uni-wuppertal.de/de/projekte/alphames.html) run by the
[Chair of Technologies and Management of Digital Transformation](https://www.tmdt.uni-wuppertal.de/de/startseite.html)
within the [University of Wuppertal](https://www.uni-wuppertal.de/).

It was funded via a research grant by the German Federal Ministry for Economics and Energy (BMWi).


## Disclaimer

This software is provided "as is" without warranty of any kind. See the LICENSE file for details.

Furthermore, this software is currently actively developed and used in research so that there are no guarantees
for stable interfaces etc.


## Copyright

```
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
```
