"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

class CategoryColors:

    """Helper class that returns colors useful for coloring categories (because visually distinctive)"""

    colors = ['#3949AB', '#1E88E5', '#43A047', '#FDD835', '#F4511E', '#757575']

    colors_light = ['#64B5F6', '#81C784', '#FFF176', '#FFB74D', '#CE93D8', '#E57373', '#4DB6AC', '#E0E0E0', '#DCE775', '#BCAAA4']

    @staticmethod
    def get(idx):
        #return CategoryColors.colors[idx % len(CategoryColors.colors)]
        return CategoryColors.colors_light[idx % len(CategoryColors.colors_light)]
