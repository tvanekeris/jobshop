"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from pathlib import Path


class AgentFile:

    @staticmethod
    def agent_filepath(job_count, machine_count):
        return Path('..') / 'agents' / f'drl_agent_{job_count}_{machine_count}.zip'
