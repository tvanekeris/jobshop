"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import logging

from helpers.normalization import Normalization

from drl.jsp_feature import JspFeature


class BottleneckFeature(JspFeature):

    def get_vector(self, observer):

        duration_matrix_jobs_machines = observer.duration_matrix_jobs_machines
        duration_unscheduled_per_machine = observer.duration_unscheduled_per_machine
        logging.debug(f'duration_matrix_jobs_machines: {duration_matrix_jobs_machines}')
        logging.debug(f'duration_unscheduled_per_machine: {duration_unscheduled_per_machine}')
        return Normalization.n01(Normalization.n01(duration_matrix_jobs_machines) @ Normalization.n01(duration_unscheduled_per_machine))
