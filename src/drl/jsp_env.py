"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import logging
from typing import List, Type

import numpy as np
import gym
from gym import spaces

from drl.jsp_feature import JspFeature
from helpers.normalization import Normalization

from jsp.schedule import Schedule
from jsp.observer import Observer
from jsp_source.jsp_source import JspSource
from drl.jsp_reward import JspReward


class JspEnv(gym.Env):
    """Environment for solving Job Shop Scheduling Problems (JSP)"""

    """
    Meta data:
    Not sure what this is for
    """
    metadata = {'render.modes': ['human']}

    def __init__(self, jsp_source: JspSource, features: List[Type[JspFeature]], loop_exception=False):

        super().__init__()

        # The source provides us with new JSPs on every reset
        self.jsp_source = jsp_source

        # JSP is set on reset
        self.jsp = None
        self.schedule = None
        self.observer = Observer(None)

        self.features = [feature() for feature in features]

        self.loop_exception = loop_exception

        """
        Action space:
        In each timestep, the agent is asked to take a scheduling decision.
        The agent chooses the job from which to schedule the next task, so the action space
        depends on job count in the JSP.
        """
        self.action_space = spaces.Discrete(self.jsp_source.job_count)

        """
        Observation space:
        Size depends on job count in JSP and count of observation vectors
        """
        self.observation_space = spaces.Box(low=0.0, high=np.inf, shape=(self.jsp_source.job_count, len(self.features)), dtype=np.float32)

        """
        Reward range:
        See below, the maximum reward is given when the deviation between our makespan and the optimal makespan
        is 0.
        """
        # TODO: Check between negative reward given when wrong action is chosen
        self.reward_range = (-float('inf'), JspReward.get_reward(1.0, 1.0))

        self.optimal = None
        self.max_reward = 0.0
        self._last_schedule = None

        self.scheduled_job_chosen_count = 0

        self.env_resets_count = 0
        self.action_count = 0

    @property
    def last_schedule(self) -> Schedule:
        return self._last_schedule

    def get_observation_from_schedule(self):

        observation_vectors = np.concatenate([feature.get_vector(self.observer) for feature in self.features], axis=1)

        return observation_vectors

        duration_unscheduled_per_job = self.observer.duration_unscheduled_per_job
        logging.debug(f'duration_unscheduled_per_job: {duration_unscheduled_per_job}')
        indicator_duration_unscheduled_per_job = Normalization.n01(duration_unscheduled_per_job)

        task_count_unscheduled_per_job = self.observer.task_count_unscheduled_per_job
        logging.debug(f'task_count_unscheduled_per_job: {task_count_unscheduled_per_job}')
        indicator_task_count_unscheduled_per_job = Normalization.n01(task_count_unscheduled_per_job)

        duration_matrix_jobs_machines = self.observer.duration_matrix_jobs_machines
        duration_unscheduled_per_machine = self.observer.duration_unscheduled_per_machine
        logging.debug(f'duration_matrix_jobs_machines: {duration_matrix_jobs_machines}')
        logging.debug(f'duration_unscheduled_per_machine: {duration_unscheduled_per_machine}')
        indicator_bottleneck = Normalization.n01(Normalization.n01(duration_matrix_jobs_machines) @ Normalization.n01(duration_unscheduled_per_machine))

        # Apparently the duration of the next task is a bad indicator that does not help the agent
        duration_next_task_per_job = self.observer.duration_next_task_per_job
        logging.debug(f'duration_next_task_per_job: {duration_next_task_per_job}')
        indicator_duration_next_task_per_job = Normalization.n01(duration_next_task_per_job)

        slack = self.observer.slack_introduced_per_job
        logging.debug(f'slack: {slack}')
        indicator_slack = Normalization.n01(slack)

        """
        IDEAS for further indicators:

        - Matrix which job selects which machine
        - Vector that shows the relative remaining duration of each machine
        """

        observation_vectors = np.concatenate((
            #indicator_duration_next_task_per_job,

            indicator_duration_unscheduled_per_job,
            indicator_bottleneck,
            indicator_task_count_unscheduled_per_job,

            #indicator_slack
        ), axis=1)

        return observation_vectors

    def reset(self):

        self.jsp = self.jsp_source.next()
        self.schedule = Schedule(self.jsp)
        self.observer.schedule = self.schedule

        self.env_resets_count += 1

        obs = self.get_observation_from_schedule()

        logging.debug(f'Rst: Observeration: {obs}')

        self.max_reward = 0.0

        return obs

    def step(self, action: np.int64):
        self.action_count += 1

        reward = 0.0

        if action >= 0:
            job_idx = action
            job = self.schedule.jobs[job_idx]
            if job.idx != job_idx:
                raise Exception('Construction of jobs is wrong. jobs[job_idx].idx != job_idx')
            if job.scheduled:
                # Give a large negative reward if the agent chooses a job in which all tasks are already scheduled
                reward = -200.0
                if self.loop_exception:
                    self.scheduled_job_chosen_count += 1
                    if self.scheduled_job_chosen_count > 20:
                        # TODO: Maybe chose a valid action here
                        raise Exception(f'JSP {self.jsp.name}: Action loop detected!')
            else:
                task = job.next_task_to_schedule
                task.schedule_auto()
                self.scheduled_job_chosen_count = 0
        else:
            raise Exception('Action is < 0. Forbidden!')

        # Check if JSP is finished

        done = False
        if self.schedule.scheduled:
            done = True

            reward = JspReward.get_reward(self.schedule.makespan, self.schedule.makespan_lower_bound)

            self._last_schedule = self.schedule

        obs = self.get_observation_from_schedule()

        logging.debug(f'Env: Action: {action}')
        logging.debug(f'Env: Observeration: {obs}')
        logging.debug(f'Env: Done: {done} / Reward: {reward}')

        if done:
            logging.debug(f'{self.jsp.task_count} tasks were scheduled with {self.action_count} actions.')
            self.action_count = 0

        if reward > self.max_reward:
            self.max_reward = reward
            logging.debug(f'Env: Maximum reward: {self.max_reward}')

        info = {}

        if done:
            obs = self.reset()
        return obs, reward, done, info

    def render(self, mode='human'):
        print(self.jsp)
        print(self.last_schedule)

    def close(self):
        pass

    def seed(self, seed=None):
        return
