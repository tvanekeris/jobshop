"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from abc import abstractmethod

from jsp.observer import Observer


class JspFeature:

    @abstractmethod
    def get_vector(self, observer):
        pass
