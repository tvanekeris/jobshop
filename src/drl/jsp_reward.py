"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import math


class JspReward:

    @staticmethod
    def get_reward(makespan_actual, makespan_comparison) -> float:

        reward = (-math.log(makespan_actual / makespan_comparison - 0.95) + 2.5) * 50

        #reward = -math.exp(makespan_actual / makespan_comparison / 2.0) + 250

        # Tests of different reward functions
        # We want to be maximum 20% worse then the optimal solution
        # reward = 1.0/float(self.jsp.makespan - self.optimal + 1)
        # reward = math.log(self.optimal / self.jsp.makespan * 1.5) * 20
        # reward = math.log(self.optimal / self.jsp.makespan * 1.5) * 50
        # reward = math.log2(self.optimal / self.jsp.makespan * 1.15) * 40

        """
            Interessant: Zwischen diesen beiden Reward-Funktionen ist es so, dass der Agent bei b)
            oft Aktionen wählt, die den Zustand nicht weiter treiben (ungültige Aktionen), um einer Bestrafung
            zu entgehen

            a) reward = math.log(self.jsp.makespan_optimal / self.jsp.makespan * 1.2) * 50
            b) reward = math.log(self.optimal / self.jsp.makespan * 1.15) * 60

            Denn bei sehr schlechten Lösungen (doppelter optimaler Makespan) gibt
            a) math.log(0.5 * 1.2) * 50  = -25.54
            b) math.log(0.5 * 1.15) * 60 = -33.20

            also b) gibt schon einen stärker negativen Reward
        """
        # The first version used the optimal makespan in the reward calculation. But in the real world, we do not
        # know the optimal makespan (because we cannot run an optimal solver in parallel) so now the reward does
        # only rely on the lower bound of the makespan that we can easily calculate when seeing the problem
        # reward = math.log(self.jsp.makespan_optimal / self.jsp.makespan * 1.2) * 50

        # reward = math.log(self.jsp.makespan_lower_bound / self.jsp.makespan * 1.2) * 50 + 100

        # reward = -math.log(self.jsp.makespan - self.jsp.makespan_lower_bound + 1)

        return reward
