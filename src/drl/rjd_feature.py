"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import logging

from helpers.normalization import Normalization

from drl.jsp_feature import JspFeature


class RjdFeature(JspFeature):

    def get_vector(self, observer):

        duration_unscheduled_per_job = observer.duration_unscheduled_per_job
        logging.debug(f'duration_unscheduled_per_job: {duration_unscheduled_per_job}')
        return Normalization.n01(duration_unscheduled_per_job)
