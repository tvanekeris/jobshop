"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import logging

from helpers.normalization import Normalization

from drl.jsp_feature import JspFeature


class RtcFeature(JspFeature):

    def get_vector(self, observer):

        task_count_unscheduled_per_job = observer.task_count_unscheduled_per_job
        logging.debug(f'task_count_unscheduled_per_job: {task_count_unscheduled_per_job}')
        return Normalization.n01(task_count_unscheduled_per_job)
