"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import logging

from helpers.normalization import Normalization

from drl.jsp_feature import JspFeature


class SlackFeature(JspFeature):

    def get_vector(self, observer):

        slack = observer.slack_introduced_per_job
        logging.debug(f'slack: {slack}')
        return Normalization.n01(slack)
