"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import numpy as np


class MatrixTools:

    @staticmethod
    def distance_matrix_from_upper_half_wo_diagonal_vector(self, vector, size):
        matrix = np.zeros([size, size], dtype=float)
        matrix_upper_indices = np.triu_indices(size, 1)
        vector_index = 0
        for matrix_index in np.array(matrix_upper_indices).transpose():
            print(f'Writing to {matrix_index} from {vector_index}')
            matrix[matrix_index[0], matrix_index[1]] = vector[vector_index]
            matrix[matrix_index[1], matrix_index[0]] = vector[vector_index]
            vector_index += 1
        return matrix