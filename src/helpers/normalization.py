"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import numpy as np


class Normalization:

    @staticmethod
    def n01(values):
        # Normalize values to [0; 1]:
        maximum_value = values.max()
        if maximum_value > 0.0:
            # This is important, otherwise for a null vector all elements are set to 1
            minimum_value = values.min()
            return np.interp(values, (minimum_value, maximum_value), (0.0, 1.0))
        return values

    @staticmethod
    def n11(values):
        # Normalize values to [-1; 1]:
        # Old observation: Normalizing to [-1; 1] leads to an agent that always takes the job that is already completely scheduled
        maximum_value = values.max()
        if maximum_value > 0.0:
            # This is important, otherwise for a null vector all elements are set to 1
            minimum_value = values.min()
            return np.interp(values, (minimum_value, maximum_value), (-1.0, 1.0))
        return values
