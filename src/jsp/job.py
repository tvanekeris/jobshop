"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from __future__ import annotations

from typing import Optional, List

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from jsp.scheduled_task import ScheduledTask


class Job:

    """Class for a job in a job shop scheduling problem containing multiple tasks"""

    def __init__(self, idx):
        self._idx = idx
        self._tasks = []

    def __str__(self):
        output = ''
        for task in self.tasks:
            output += str(task) + ' '
        return output

    @property
    def idx(self) -> int:
        return self._idx

    @property
    def tasks(self) -> List[ScheduledTask]:
        return self._tasks

    def add_task(self, task: ScheduledTask) -> None:
        self._tasks.append(task)

    @property
    def task_count(self) -> int:
        return len(self.tasks)

    @property
    def task_count_unscheduled(self) -> int:
        task = self.next_task_to_schedule
        if not task:
            return 0
        return self.task_count - task.idx

    @property
    def duration(self) -> int:
        return sum([task.duration for task in self.tasks])

    @property
    def duration_unscheduled(self) -> int:
        duration_unscheduled = 0
        for task in self.tasks:
            if not task.scheduled:
                duration_unscheduled += task.duration
        return duration_unscheduled

    @property
    def scheduled(self) -> bool:
        if self.next_task_to_schedule:
            return False
        else:
            return True

    @property
    def next_task_to_schedule(self) -> Optional[ScheduledTask]:
        for task in self.tasks:
            if not task.scheduled:
                return task
        # Job is already completely scheduled, there is no next task
        return None

    def calculate_earliest_starts(self) -> None:
        # This ensures sequentiality during scheduling
        current_end = 0
        for task in self.tasks:
            task.earliest_start = current_end
            if task.scheduled:
                current_end = task.end
            else:
                current_end += task.duration

