"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from __future__ import annotations

import os
import re

from typing import List, Union

from jsp.jsp_base import JspBase
from jsp.task import Task

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from jsp.schedule import Schedule


class Jsp(JspBase):

    """Job shop scheduling problem (JSP): Problem definition"""

    def __init__(self, job_count: int = 0, machine_count: int = 0):

        super().__init__()

        self._job_count = job_count
        self._machine_count = machine_count

        self._tasks = []

        self._schedules = []

    @property
    def job_count(self) -> int:
        return self._job_count

    @property
    def machine_count(self) -> int:
        return self._machine_count

    @property
    def tasks(self) -> List[Task]:
        return self._tasks

    @property
    def task_count(self) -> int:
        return len(self._tasks)

    # Not required so far
    """
    def add_schedules(self, schedules: Union[Schedule, List[Schedule]]) -> None:

        if type(schedules) is not list:
            schedules = [schedules]

        self._schedules += schedules
    """

    def load_from_file(self, filepath, includes_header=True, job_count=None, machine_count=None) -> None:
        with open(filepath, 'r') as file:
            text = file.read()

        self.init_from_or_format(text, includes_header, job_count, machine_count)

        self._name = os.path.splitext(filepath)[0]

    def save_to_file(self, filepath):
        with open(filepath, 'w') as file:
            file.write(self.or_format_string)

    @property
    def or_format_string(self) -> str:

        """
        Job data format from OR library

        Each instance consists of a line of description, a line containing the
        number of jobs and the number of machines, and then one line for each job,
        listing the machine number and processing time for each step of the job.
        The machines are numbered starting with 0.

        3 3
        0 3 1 2 2 2
        0 2 2 1 1 4
        1 4 2 3
        """

        machine_str_length = len(str(self.machine_count-1))
        duration_str_length = len(str(max([task.duration for task in self.tasks])))

        text = f'{self.job_count} {self.machine_count}\n'
        job_idx = 0
        for task in self.tasks:
            if task.job_idx > job_idx:
                job_idx += 1
                text += '\n'
            if task.idx > 0:
                text += ' '
            text += f'{task.machine_idx}'.rjust(machine_str_length)
            text += ' '
            text += f'{task.duration}'.rjust(duration_str_length)

        if self.makespan_optimal is not None:
            text += f'\n{self.makespan_optimal}'

        return text

    def init_from_or_format(self, or_format_text, includes_header=True, job_count=None, machine_count=None):

        # Reset the JSP first
        self.__init__()

        lines = iter(or_format_text.splitlines())

        if includes_header:
            line = next(lines)
            counters = re.search(r'(\d+)\s+(\d+)', line).groups()
            job_count_from_header = int(counters[0])
            machine_count_from_header = int(counters[1])
        else:
            job_count_from_header = job_count
            machine_count_from_header = machine_count

        for job_idx in range(job_count_from_header):
            line = next(lines)
            tasks = []
            for task_idx, task_data in enumerate(re.finditer(r'(\d+)\s+(\d+)', line)):
                machine_idx = int(task_data.group(1))
                if machine_idx > machine_count_from_header - 1:
                    raise Exception(f'OR format corrupt. The header states {machine_count_from_header} machines but the data refers to machine index {machine_idx}')
                duration = int(task_data.group(2))
                task = Task(task_idx, job_idx, machine_idx, duration)

                tasks.append(task)
            if not tasks:
                raise Exception('OR format corrupt. There are not enough data lines for the job count given in header.')
            self._tasks += tasks

        line = next(lines, '')
        match = re.search(r'\d+', line)
        if match:
            self.makespan_optimal = int(match[0])

        # Computes number of machines from data
        machine_count_from_data = 1 + max(task.machine_idx for task in self.tasks)
        if not machine_count_from_header == machine_count_from_data:
            raise Exception(f'OR format corrupt. Header states {machine_count_from_header} machines but data only uses {machine_count_from_data} machines.')

        self._job_count = job_count_from_header
        self._machine_count = machine_count_from_header

    def __eq__(self, other: Jsp):
        if self.job_count != other.job_count:
            return False
        if self.machine_count != other.machine_count:
            return False
        if self.task_count != other.task_count:
            return False
        for task_idx in range(self.task_count):
            if self.tasks[task_idx].machine_idx != other.tasks[task_idx].machine_idx:
                return False
            if self.tasks[task_idx].duration != other.tasks[task_idx].duration:
                return False
        return True

    def __str__(self):
        text = 'JSP '
        if self.name:
            text += f'({self.name}) '
        text += f'with {self.job_count} jobs, {self.machine_count} machines and horizon {self.horizon}'

        return text

