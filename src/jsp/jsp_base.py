"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from abc import abstractmethod
import numpy as np
from typing import Optional, List
from jsp.task import Task


class JspBase:

    """Base class for JSPs. Used by classes Jsp and Schedule"""

    def __init__(self):
        self._name = None

        self._makespan_optimal = None

    @property
    def name(self) -> Optional[str]:
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    @abstractmethod
    def job_count(self) -> int:
        pass

    @property
    @abstractmethod
    def machine_count(self) -> int:
        pass

    @property
    @abstractmethod
    def tasks(self) -> List[Task]:
        pass

    @property
    @abstractmethod
    def task_count(self) -> int:
        pass

    @property
    def makespan_optimal(self) -> int:
        return self._makespan_optimal

    @makespan_optimal.setter
    def makespan_optimal(self, value) -> None:
        if self._makespan_optimal is not None:
            raise(Exception('Trying to overwrite optimal makespan which was already present!'))
        self._makespan_optimal = value

    @property
    def makespan_lower_bound(self):
        machine_durations = np.zeros(self.machine_count, dtype=int)

        for task in self.tasks:
            machine_durations[task.machine_idx] += task.duration

        return max(machine_durations)

    @property
    def horizon(self) -> int:
        # Computes horizon dynamically as the sum of all durations
        return sum(task.duration for task in self.tasks)
