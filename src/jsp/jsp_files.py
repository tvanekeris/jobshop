"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import os.path
from pathlib import Path
from typing import List
import logging

from tqdm import tqdm

from jsp.jsp import Jsp


class JspFiles:

    FILENAME_LENGTH = 10

    @staticmethod
    def data_directory_raw() -> Path:
        return Path('..') / 'data'

    @staticmethod
    def data_directory(job_count: int, machine_count: int) -> Path:
        return JspFiles.data_directory_raw() / f'{job_count}_{machine_count}'

    @staticmethod
    def data_filename(problem_idx: int) -> Path:
        return Path(f'{problem_idx}'.zfill(JspFiles.FILENAME_LENGTH) + '.dat')

    @staticmethod
    def data_filepath(job_count: int, machine_count: int, problem_idx: int) -> Path:
        return JspFiles.data_directory(job_count, machine_count) / JspFiles.data_filename(problem_idx)

    @staticmethod
    def load_jsp_list_from_dir(directory: Path, includes_header=True, job_count=None, machine_count=None) -> List[Jsp]:
        logging.info(f'Loading all problems from directory "{directory}")')

        jsps = []

        for filename in tqdm(os.listdir(directory)):
            jsp = Jsp()
            # For Vladimir's data:
            #jsp.load_from_file(directory / filename, includes_header=False, job_count=6, machine_count=6)
            # For Tilo's data:
            # jsp.load_from_file(directory / filename, includes_header=True)
            jsp.load_from_file(directory / filename, includes_header=includes_header, job_count=job_count, machine_count=machine_count)
            jsps.append(jsp)

        return jsps

    @staticmethod
    def load_jsp_list(job_count: int, machine_count: int, problem_start_idx: int, problem_count: int) -> List[Jsp]:
        # TODO: Rewrite as generator
        # TODO: Add parameter "enforce optimal makespan"

        problem_end_idx = problem_start_idx + problem_count

        logging.info(f'Loading problems {problem_start_idx} to {problem_end_idx} ({problem_count} problems)')

        jsps = []
        for problem_idx in tqdm(range(problem_start_idx, problem_end_idx)):
            data_filepath = JspFiles.data_filepath(job_count, machine_count, problem_idx)
            if not data_filepath.exists():
                raise(Exception(f'JSP file cannot be found: {data_filepath}. Make sure enough data files were generated.'))
            jsp = Jsp()
            jsp.load_from_file(data_filepath)
            jsps.append(jsp)

        return jsps
