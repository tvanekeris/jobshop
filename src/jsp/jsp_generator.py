"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from typing import List, Union
import numpy as np

from jsp.jsp import Jsp
from jsp.task import Task


class JspGenerator:

    def __init__(self, rng=None):
        self.rng = rng
        if self.rng is None:
            self.rng = np.random.default_rng()

    def generate(self, job_count: int, machine_count: int, jsp_count: int = 1) -> Union[Jsp, List[Jsp]]:

        jsps = []
        for jsp_idx in range(jsp_count):
            jsp = self.generate_single(job_count, machine_count)
            jsps.append(jsp)

        if jsp_count == 1:
            return jsps[0]
        else:
            return jsps

    def generate_single(self, job_count: int, machine_count: int) -> Jsp:

        # TODO: Initialize random number generator with a seed

        while True:
            used_machines = set()

            tasks = []

            for job_idx in range(job_count):

                # For task count decide if it's the same as job count or if it's sampled from a distribution
                task_count = job_count
                #task_count = max(0, round(np.random.normal(job_count)))

                for task_idx in range(task_count):
                    mean = job_count
                    sigma = mean / 4.0
                    duration = round(self.rng.normal(mean, sigma))
                    duration = max(1, duration)
                    # TODO: Test uniform distribution
                    # duration = self.rng.integers(1, job_count)
                    machine_idx = self.rng.integers(0, machine_count)
                    used_machines.add(machine_idx)
                    task = Task(task_idx, job_idx, machine_idx, duration)
                    tasks.append(task)

            # Make sure that each machine is at least used by one task
            if len(used_machines) == machine_count:
                break

        jsp = Jsp(job_count, machine_count)
        jsp._tasks = tasks

        return jsp
