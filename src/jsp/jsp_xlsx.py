"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import xlsxwriter
from xlsxwriter.exceptions import FileCreateError
from typing import List, Union

from category_colors.category_colors import CategoryColors
from jsp.jsp import Jsp
from jsp.schedule import Schedule


class JspXlsx:

    """Excel file writer for job shop problems and solutions"""

    border_thick = 2
    border_thin = 1
    cell_width = 2
    grey = '#cccccc'
    black = '#000000'

    def __init__(self, filename):
        self._filename = filename

        # Create a workbook and add a worksheet.
        self._workbook = xlsxwriter.Workbook(filename)
        self._worksheet = self.workbook.add_worksheet('JSP')

        self._job_formats = []

        self._row = 0

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        stop = False
        while not stop:
            try:
                self.workbook.close()
                stop = True
            except FileCreateError:
                print(f'The file {self.filename} cannot be opened for writing. Is it open in another application?')
                text = input('Press enter to try again or "N" to quit.')
                if text.lower() == 'n':
                    stop = True

        # TODO: Set excel file property to make file readonly
        #mode = os.stat(self.filename).st_mode
        #ro_mask = 0o777 ^ (stat.S_IWRITE | stat.S_IWGRP | stat.S_IWOTH)
        #os.chmod(self.filename, mode & ro_mask)

    @property
    def filename(self):
        return self._filename

    @property
    def workbook(self):
        return self._workbook

    @property
    def worksheet(self):
        return self._worksheet

    @property
    def row(self):
        return self._row

    @row.setter
    def row(self, value):
        self._row = value

    def create_job_formats(self, jsp: Jsp):

        for job_idx in range(len(self._job_formats), jsp.job_count):

            job_format = {'start': self.workbook.add_format(), 'middle': self.workbook.add_format(),
                          'end': self.workbook.add_format(), 'startend': self.workbook.add_format()}
            for key in job_format.keys():
                job_format[key].set_bg_color(CategoryColors.get(job_idx))
                job_format[key].set_top(JspXlsx.border_thick)
                job_format[key].set_bottom(JspXlsx.border_thick)
                job_format[key].set_font_size(8)
                job_format[key].set_align('vcenter')
                job_format[key].set_bold(True)
                job_format[key].set_right(JspXlsx.border_thin)
                job_format[key].set_right_color(JspXlsx.grey)
                if key == 'start':
                    job_format[key].set_left(JspXlsx.border_thick)
                if key == 'end':
                    job_format[key].set_right(JspXlsx.border_thick)
                    job_format[key].set_right_color(JspXlsx.black)
                if key == 'startend':
                    job_format[key].set_left(JspXlsx.border_thick)
                    job_format[key].set_right(JspXlsx.border_thick)
                    job_format[key].set_right_color(JspXlsx.black)
            self._job_formats.append(job_format)

    def draw_task(self, col_list, job_format, text):
        for col in col_list:
            cell_text = ''
            if len(col_list) == 1:
                cell_format = job_format['startend']
                cell_text = text
            else:
                if col == col_list[0]:
                    cell_format = job_format['start']
                    cell_text = text
                elif col == col_list[-1]:
                    cell_format = job_format['end']
                else:
                    cell_format = job_format['middle']
            self.worksheet.write(self.row, col + 1, cell_text, cell_format)

    def set_workbook_options(self):
        self.workbook.set_tab_ratio(10)

    def set_worksheet_options(self):
        pass

    def write_jsp(self, jsp: Jsp):

        self.create_job_formats(jsp)

        self.worksheet.write(self.row, 0, str(jsp))

        start = 0
        job_format = None
        for task in jsp.tasks:
            if task.idx == 0:
                self.row += 1
                start = 0
                job_format = self._job_formats[task.job_idx]
                self.worksheet.write(self.row, 0, f'Job {task.job_idx}')

            col_list = list(range(start, start+task.duration))
            text = f'{task.id} (M: {task.machine_idx})'

            self.draw_task(col_list, job_format, text)

            start = start+task.duration

        self.row += 2

        self.worksheet.set_column(0, 0, 20)
        self.worksheet.set_column(1, jsp.horizon, JspXlsx.cell_width)
        self.set_workbook_options()

    def write_schedules(self, schedules: Union[Schedule, List[Schedule]]):

        if type(schedules) is not list:
            schedules = [schedules]

        for schedule in schedules:

            if not schedule.scheduled:
                raise(Exception('Cannot write solution for this JSP. The JSP has not been solved.'))

            self.create_job_formats(schedule)

            text = f'{schedule.name} solution (makespan {schedule.makespan}; '
            if schedule.calc_time:
                text += f'time {schedule.calc_time:.3f}s)'
            self.worksheet.write(self.row, 0, text)
            self.row += 1
            for machine in schedule.machines:
                self.worksheet.write(self.row, 0, f'Machine {machine.idx}')
                for task in machine.tasks_scheduled:
                    job_format = self._job_formats[task.job.idx]
                    col_list = list(range(task.start, task.end))
                    text = task.id

                    self.draw_task(col_list, job_format, text)

                self.row += 1

            self.row += 1

        self.set_workbook_options()
        self.worksheet.set_column(0, 0, 12)
        self.worksheet.set_column(1, max([jsp.makespan for jsp in schedules]), JspXlsx.cell_width)
