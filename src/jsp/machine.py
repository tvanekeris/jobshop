"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from __future__ import annotations

from typing import List

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from jsp.scheduled_task import ScheduledTask


class Machine:

    """Class for a machine in a job shop scheduling problem"""

    def __init__(self, idx: int):
        self._idx = idx
        self._tasks = []
        self._tasks_scheduled: List[ScheduledTask] = []

    @property
    def idx(self) -> int:
        return self._idx

    @property
    def tasks(self) -> List[ScheduledTask]:
        # Tasks of the machine in order of addition
        return self._tasks

    def add_task(self, task: ScheduledTask) -> None:
        self._tasks.append(task)

    @property
    def tasks_scheduled(self) -> List[ScheduledTask]:
        # Scheduled tasks in scheduled order
        self._tasks_scheduled.sort(key=lambda task: task.start)
        return self._tasks_scheduled

    @property
    def earliest_start(self) -> int:
        if not self.tasks_scheduled:
            return 0
        else:
            return self.tasks_scheduled[-1].end

    @property
    def task_count(self) -> int:
        return len(self.tasks)

    @property
    def task_count_scheduled(self) -> int:
        return len(self.tasks_scheduled)

    @property
    def task_count_unscheduled(self) -> int:
        return self.task_count - self.task_count_scheduled

    @property
    def duration(self) -> int:
        """
        Returns the cumulative duration of all tasks that belong to this machine.
        This does NOT take the starting values of tasks into account.

        :return: the duration
        """
        duration = 0
        for task in self.tasks:
            duration += task.duration
        return duration

    @property
    def duration_scheduled(self) -> int:
        """
        Returns the cumulative duration of all tasks that belong to this machine AND are scheduled.
        This does not take the starting values of tasks into account.

        :return: the duration
        """
        duration_scheduled = 0
        for task in self.tasks_scheduled:
            duration_scheduled += task.duration
        return duration_scheduled

    @property
    def duration_unscheduled(self) -> int:
        """
        Returns the cumulative duration of all tasks that belong to this machine AND are NOT YET scheduled.
        This does not take the starting values of tasks into account.

        :return: the duration
        """
        return self.duration - self.duration_scheduled

    def schedule_auto(self, task: ScheduledTask) -> int:
        # Schedule task as good as possible by starting with the earliest start of the task to ensure sequentiality
        # and then iteratively moving the task to later starts until there is no overlap on the machine

        # Initialize the start timestep with the earliest timestep possible from the task (this ensures the task sequentiality)
        start = task.earliest_start
        # Iteratively filter for already scheduled tasks that overlap with the task to be scheduled
        while True:
            overlapping_tasks = list(filter(lambda scheduled_task: (start <= scheduled_task.start < start + task.duration) or (scheduled_task.start <= start < scheduled_task.end), self.tasks_scheduled))
            if overlapping_tasks:
                start = max(task.end for task in overlapping_tasks)
            else:
                break

        self._tasks_scheduled.append(task)
        return start

    def schedule_manual(self, task: ScheduledTask) -> None:
        if not task.machine == self:
            raise(Exception(f'Machine {self.idx}: A task with required machine {task.machine.idx} cannot be scheduled on this machine'))
        if task not in self.tasks:
            raise (Exception(f'Machine {self.idx}: A task that wants to be scheduled on me needs to be in my task list first'))
        self._tasks_scheduled.append(task)

    def unschedule_task(self, task: ScheduledTask):
        raise NotImplementedError
        #if task in self.tasks_scheduled:
        #    self._tasks_scheduled.remove(task)

    def unschedule_all(self):
        raise NotImplementedError
        #self._tasks_scheduled = []

    def __str__(self):
        output = f'Machine {self.idx}: '
        for task in self.tasks_scheduled:
            output += str(task) + ' '
        return output
