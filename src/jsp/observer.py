"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import numpy as np
import typing

from jsp.schedule import Schedule


class Observer:

    def __init__(self, schedule: typing.Optional[Schedule]):
        self._schedule = schedule

    @property
    def schedule(self):
        return self._schedule

    @schedule.setter
    def schedule(self, value: Schedule):
        self._schedule = value

    @property
    def schedule_status_per_job(self):
        job_valid = np.ones((self.schedule.job_count, 1), dtype=np.float32)
        for job in self.schedule.jobs:
            if job.next_task_to_schedule is None:
                job_valid[job.idx] = 0
        return job_valid

    @property
    def duration_unscheduled_per_job(self):
        # Computes the unscheduled total duration per job as column vector
        duration_unscheduled_per_job = np.zeros((self.schedule.job_count, 1), dtype=np.float32)
        for job in self.schedule.jobs:
            for task in job.tasks:
                if not task.scheduled:
                    duration_unscheduled_per_job[job.idx, 0] += task.duration
        return duration_unscheduled_per_job

    @property
    def duration_next_task_per_job(self):
        # Computes the duration of the next-to-be-scheduled task per job as column vector
        duration_next_task_per_job = np.zeros((self.schedule.job_count, 1), dtype=np.float32)
        for job in self.schedule.jobs:
            if job.next_task_to_schedule is not None:
                duration_next_task_per_job[job.idx, 0] = job.next_task_to_schedule.duration
        return duration_next_task_per_job

    @property
    def task_count_unscheduled_per_job(self):
        # Computes the count of unscheduled tasks per job as column vector
        task_count_unscheduled_per_job = np.zeros((self.schedule.job_count, 1), dtype=np.float32)
        for job in self.schedule.jobs:
            for task in job.tasks:
                if not task.scheduled:
                    task_count_unscheduled_per_job[job.idx, 0] += 1.0
        return task_count_unscheduled_per_job

    @property
    def duration_unscheduled_per_machine(self):
        # Computes the unscheduled duration per machine as column vector
        duration_unscheduled_per_machine = np.zeros((self.schedule.machine_count, 1), dtype=np.float32)
        for machine in self.schedule.machines:
            for task in machine.tasks:
                if not task.scheduled:
                    duration_unscheduled_per_machine[machine.idx, 0] += task.duration
        return duration_unscheduled_per_machine

    @property
    def duration_matrix_jobs_machines(self):
        """
        Shows how much unscheduled time there is for each job on each machine
        When this is multiplied with a vector that contains info per machine,
        it "translates" the importance to the job

                 machines
                 0  0  0
        jobs     0  0  0
                 0  0  0
        """
        duration_matrix_jobs_machines = np.zeros((self.schedule.job_count, self.schedule.machine_count), dtype=np.float32)
        for job in self.schedule.jobs:
            for task in job.tasks:
                if not task.scheduled:
                    duration_matrix_jobs_machines[job.idx, task.machine.idx] += task.duration
        return duration_matrix_jobs_machines

    @property
    def slack_introduced_per_job(self):
        slack_introduced_per_job = np.zeros((self.schedule.job_count, 1), dtype=np.float32)
        for job in self.schedule.jobs:
            task = job.next_task_to_schedule
            if task is not None:
                slack_introduced_per_job[job.idx, 0] = max(task.earliest_start - task.machine.earliest_start, 0)
        return slack_introduced_per_job

    @property
    def matrix_job_next_machine(self):
        raise NotImplementedError
        """
        job_machine_matrix = np.zeros((self.jsp_source.job_count, self.jsp_source.machine_count), dtype=np.float32)
        for job in self.jsp.jobs:
            task = job.next_task_to_schedule
            if task is not None:
                job_machine_matrix[job.idx, job.next_task_to_schedule.machine.idx] = 1
        """

