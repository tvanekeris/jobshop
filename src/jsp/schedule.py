"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from __future__ import annotations

from typing import Optional, List

from jsp.jsp_base import JspBase
from jsp.job import Job
from jsp.machine import Machine

from jsp.scheduled_task import ScheduledTask


class Schedule(JspBase):

    """Schedule (solution) to a JSP"""

    def __init__(self, jsp):

        super().__init__()

        self._calc_time = None

        # Add reference to the JSP
        self.jsp = jsp

        self._jobs = [Job(job_idx) for job_idx in range(self.jsp.job_count)]
        self._machines = [Machine(machine_idx) for machine_idx in range(self.jsp.machine_count)]

        for task in jsp.tasks:
            job = self.jobs[task.job_idx]
            machine = self.machines[task.machine_idx]
            job.tasks.append(ScheduledTask(task.idx, job, machine, task.duration))

        self.calculate_earliest_starts()

        self._makespan_optimal = jsp.makespan_optimal

    @property
    def jobs(self) -> List[Job]:
        return self._jobs

    @property
    def job_count(self) -> int:
        return len(self._jobs)

    def add_job(self, job: Job) -> None:
        self._jobs.append(job)

    @property
    def machines(self) -> List[Machine]:
        return self._machines

    @property
    def machine_count(self) -> int:
        return len(self._machines)

    def add_machine(self, machine: Machine) -> None:
        self._machines.append(machine)

    @property
    def tasks(self) -> List[ScheduledTask]:
        tasks = []
        for job in self.jobs:
            tasks += job.tasks
        return tasks

    @property
    def task_count(self) -> int:
        return sum([job.task_count for job in self.jobs])

    def calculate_earliest_starts(self) -> None:
        for job in self.jobs:
            job.calculate_earliest_starts()

    @property
    def scheduled(self) -> bool:
        if not self.job_count > 0:
            return False
        for job in self.jobs:
            if not job.scheduled:
                return False
        return True

    @property
    def makespan(self) -> Optional[int]:
        if self.scheduled:
            return max(job.tasks[-1].end for job in self.jobs)
        else:
            return None

    @property
    def calc_time(self) -> float:
        return self._calc_time

    @calc_time.setter
    def calc_time(self, value) -> None:
        self._calc_time = value

    @property
    def valid(self) -> bool:
        # All tasks are scheduled
        for job in self.jobs:
            for task in job.tasks:
                if not task.scheduled:
                    raise Exception(f'Schedule is not valid. Task {task.id} is not scheduled.')

        # All scheduled tasks are scheduled on the correct machine
        for machine in self.machines:
            for task in machine.tasks_scheduled:
                if not task.machine == machine:
                    raise Exception(f'Schedule is not valid. Task {task.id} requires machine {task.machine.idx} but is scheduled on machine {machine.idx}')

        # No tasks overlap
        for machine in self.machines:
            for task in machine.tasks_scheduled:
                for other_task in machine.tasks_scheduled:
                    if task == other_task:
                        continue
                    if task.start <= other_task.start < task.end:
                        raise Exception(f'Schedule is not valid. Task {task.id} and {other_task.id} overlap on machine {machine.idx}')

        return True

    def reset(self) -> None:
        raise NotImplementedError
        # Unsolve a JSP, that means unscheduling all tasks
        for task in self.tasks:
            # This will call the unschedule functions on the machines as well
            task.unschedule()
        self._name = None
        self._calc_time = None

    def __str__(self):

        text = ''
        if self.name:
            text += f'{self.name} '
        text += 'Schedule; '
        if self.scheduled:
            text += 'scheduled; '
        else:
            text += 'unscheduled; '
        if self.valid:
            text += 'valid; '
        else:
            text += 'invalid; '
        if self.calc_time:
            text += f'calc time {self.calc_time:.3f}s; '
        text += f'makespan: {self.makespan}'

        return text
