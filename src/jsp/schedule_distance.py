"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from abc import abstractmethod

import logging

from jsp.schedule import Schedule


class ScheduleDistance:

    @classmethod
    @abstractmethod
    def _measure(cls, schedule1: Schedule, schedule2: Schedule) -> int:
        pass

    @classmethod
    def measure(cls, schedule1: Schedule, schedule2: Schedule) -> int:

        if not schedule1.scheduled:
            raise Exception('The first Schedule is not scheduled. Cannot compare.')
        if not schedule2.scheduled:
            raise Exception('The second Schedule is not scheduled. Cannot compare.')
        if not schedule1.jsp == schedule2.jsp:
            raise Exception('The two schedules do not belong to the same JSP.')

        return cls._measure(schedule1, schedule2)


class ScheduleDistanceMakespan(ScheduleDistance):

    @classmethod
    def _measure(cls, schedule1: Schedule, schedule2: Schedule) -> int:
        return schedule1.makespan - schedule2.makespan


class ScheduleDistanceTaskCount(ScheduleDistance):

    @classmethod
    def _measure(cls, schedule1: Schedule, schedule2: Schedule):

        differently_scheduled_tasks = 0

        for job in schedule1.jobs:
            for task in job.tasks:
                if task.start != schedule2.jobs[job.idx].tasks[task.idx].start:
                    differently_scheduled_tasks += 1

        if differently_scheduled_tasks == 0:
            logging.debug(f'Schedule equality (differently scheduled tasks): {schedule1.jsp.name}')

        return differently_scheduled_tasks


class ScheduleDistanceTaskStart(ScheduleDistance):

    @classmethod
    def _measure(cls, schedule1: Schedule, schedule2: Schedule) -> int:
        start_time_deviation = 0
        for job in schedule1.jobs:
            for task in job.tasks:
                # TODO: Check which version is better:
                # start_time_deviation += abs(task.start - schedule2.jobs[job.idx].tasks[task.idx].start)
                start_time_deviation += abs(task.start - schedule2.jobs[job.idx].tasks[task.idx].start) / schedule1.task_count
        return start_time_deviation


class ScheduleDistancePermutations(ScheduleDistance):

    @classmethod
    def _measure(cls, schedule1: Schedule, schedule2: Schedule) -> int:

        permutations = 0

        for machine_idx in range(schedule1.machine_count):
            s1_order = [task.id for task in schedule1.machines[machine_idx].tasks_scheduled]
            s2_order = [task.id for task in schedule2.machines[machine_idx].tasks_scheduled]

            if not len(s1_order) == len(s2_order):
                raise Exception(f'The task lists do not have the same length!')

            logging.debug(s1_order)
            logging.debug(s2_order)

            # Bring s2 in the order of s1 by pulling elements to the right place
            for idx in range(len(s1_order)):

                if not s1_order[idx] == s2_order[idx]:
                    s2_order.insert(idx, s2_order.pop(s2_order.index(s1_order[idx])))
                    permutations += 1

            logging.debug(s1_order)
            logging.debug(s2_order)
            logging.debug(permutations)

        if permutations == 0:
            logging.debug(f'Equivalent by measure of schedule permutations: {schedule1.jsp.name}')

        permutations_other_way = 0
        for machine_idx in range(schedule2.machine_count):
            s1_order = [task.id for task in schedule1.machines[machine_idx].tasks_scheduled]
            s2_order = [task.id for task in schedule2.machines[machine_idx].tasks_scheduled]

            # Bring s1 in the order of s2 by pulling elements to the right place
            for idx in range(len(s2_order)):

                if not s1_order[idx] == s2_order[idx]:
                    s1_order.insert(idx, s1_order.pop(s1_order.index(s2_order[idx])))
                    permutations_other_way += 1

        # TODO: Solve problem that permutation calculation is not commutative

        """
            Example:
            Sequence 1: T1 T2 T3 T4
            Sequence 2: T2 T3 T4 T1
            Bringing Sequence 2 to Sequence 1 costs 1 operation
            Bringing Sequence 1 to Sequence 2 costs 3 operations

            Idea: Pull longest correct subsequence with the element
            or alternative: Move "wrong element" to first position where it does not destroy correct sequence
        """

        # return (permutations + permutations_other_way) / 2
        return min(permutations, permutations_other_way)
