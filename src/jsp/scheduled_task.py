"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from __future__ import annotations

from jsp.task import Task

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from jsp.job import Job
    from jsp.machine import Machine


class ScheduledTask(Task):

    """Class for a task in a job shop scheduling problem"""

    def __init__(self, idx: int, job: Job, machine: Machine, duration: int):

        super().__init__(idx, job.idx, machine.idx, duration)

        self._job = job
        self._machine = machine
        self._machine.add_task(self)

        self._earliest_start = None
        self._scheduled = False
        self._start = None
        self._end = None

    @property
    def job(self) -> Job:
        return self._job

    @job.setter
    def job(self, value: Job) -> None:
        self._job = value

    @property
    def machine(self) -> Machine:
        return self._machine

    @machine.setter
    def machine(self, value: Machine) -> None:
        self._machine = value

    @property
    def earliest_start(self) -> int:
        return self._earliest_start

    @earliest_start.setter
    def earliest_start(self, value):
        self._earliest_start = value

    @property
    def scheduled(self) -> bool:
        return self._scheduled

    @property
    def start(self) -> int:
        return self._start

    @property
    def end(self) -> int:
        return self.start + self.duration

    def schedule_auto(self) -> None:
        if self.scheduled:
            raise Exception(f'Task {self.id}: Cannot be scheduled because already scheduled.')
        # Schedule task on machine as good as possible and return the start time
        start = self.machine.schedule_auto(self)
        if start < self.earliest_start:
            raise Exception(f'Task {self.id}: Cannot be scheduled with start {start} because earliest start is {self.earliest_start}.')
        self._schedule(start)

    def schedule_manual(self, start: int) -> None:
        if self.scheduled:
            raise Exception(f'Task {self.id}: Cannot be scheduled because already scheduled.')
        if start < self.earliest_start:
            raise Exception(f'Task {self.id}: Cannot be scheduled with start {start} because earliest start is {self.earliest_start}.')
        # Schedule task on machine manually
        self.machine.schedule_manual(self)
        self._schedule(start)

    def _schedule(self, start: int) -> None:
        # Schedule task itself with the start time that we got back from the machine
        self._start = start
        self._scheduled = True
        # Adapt all earliest start times of the job
        self.job.calculate_earliest_starts()

    def unschedule(self) -> None:
        # Unschedule a task
        raise NotImplementedError
        self._earliest_start = None
        self._scheduled = False
        self._start = None
        self._end = None

        # Unschedule the task from the machine as well
        self.machine.unschedule_task(self)

    def __str__(self):
        return '[T: {}, J: {}, M: {}, S: {}, D: {}]'.format(self.idx, self.job.idx, self.machine.idx, self.start, self.duration)


#    @staticmethod
#    def create_from_task(task: Task) -> ScheduledTask:
#        scheduled_task = ScheduledTask(task.idx, task.job)
