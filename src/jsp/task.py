"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

class Task:

    """Class for a task in a job shop scheduling problem"""

    def __init__(self, idx: int, job_idx: int, machine_idx: int, duration: int):

        if duration <= 0:
            raise Exception('Error when creating task: Duration cannot be <= 0')

        self._idx = idx
        self._job_idx = job_idx
        self._machine_idx = machine_idx
        self._duration = duration

    @property
    def idx(self) -> int:
        return self._idx

    @property
    def job_idx(self) -> int:
        return self._job_idx

    @property
    def id(self) -> str:
        return f'{self.job_idx}-{self.idx}'

    @property
    def machine_idx(self) -> int:
        return self._machine_idx

    @property
    def duration(self) -> int:
        return self._duration

    def __str__(self):
        return f'[T: {self.idx}, J: {self.job_idx}, M: {self.machine_idx}, D: {self.duration}]'


"""
    # TODO: Implement linked list
    self._next_in_job = None
    self._prev_in_job = None
    self._next_on_machine = None
    self._prev_on_machine = None

    @property
    def next_in_job(self) -> Task:
        return self._next_in_job

    @next_in_job.setter
    def next_in_job(self, value: Task):
        self._next_in_job = value

    @property
    def prev_in_job(self) -> Task:
        return self._prev_in_job

    @prev_in_job.setter
    def prev_in_job(self, value: Task):
        self._prev_in_job = value

    @property
    def next_on_machine(self) -> Task:
        return self._next_on_machine

    @next_on_machine.setter
    def next_on_machine(self, value: Task):
        self._next_on_machine = value

    @property
    def prev_on_machine(self) -> Task:
        return self._prev_on_machine

    @prev_on_machine.setter
    def prev_on_machine(self, value: Task):
        self._prev_on_machine = value
"""
