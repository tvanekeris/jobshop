"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from pathlib import Path

from helpers.logging_config import logging_config
from jsp.jsp import Jsp
from jsp.jsp_xlsx import JspXlsx
from schedulers.cpsat_scheduler import CpsatScheduler


def main():
    filepath_jsp = Path('..') / 'problems' / '4_4_example.dat'
    jsp = Jsp()
    jsp.load_from_file(filepath_jsp)
    schedule = CpsatScheduler(jsp).create_schedule()

    filepath_excel = Path('..') / 'xlsx' / '4_4_example.xlsx'
    with JspXlsx(filepath_excel) as jsp_xlsx:
        jsp_xlsx.write_jsp(jsp)
    jsp_xlsx.write_schedules(schedule)


if __name__ == '__main__':
    logging_config(description='Solve example JSP with CP-SAT scheduler and generate nicely formatted excel file')
    main()
