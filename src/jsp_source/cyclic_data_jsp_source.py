"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import itertools
import copy

from jsp.jsp_files import JspFiles
from jsp_source.jsp_source import JspSource


class CyclicDataJspSource(JspSource):

    def __init__(self, job_count, machine_count, problem_start_idx, problem_count):

        super().__init__(job_count, machine_count)

        self.jsps = JspFiles.load_jsp_list(job_count, machine_count, problem_start_idx, problem_count)

        self.jsp_iterator = itertools.cycle(self.jsps)

    def _next(self):
        # We don't need deepcopies any more, because the JSP does not include the schedule anymore
        return next(self.jsp_iterator)
