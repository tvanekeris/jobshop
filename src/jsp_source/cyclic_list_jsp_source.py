"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from typing import List
import itertools

from jsp.jsp import Jsp
from jsp_source.jsp_source import JspSource


class CyclicListJspSource(JspSource):

    def __init__(self, jsps: List[Jsp]):

        super().__init__(jsps[0].job_count, jsps[0].machine_count)

        self.jsp_iterator = itertools.cycle(jsps)

    def _next(self):
        return next(self.jsp_iterator)
