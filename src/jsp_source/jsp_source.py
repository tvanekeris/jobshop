"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from abc import ABC, abstractmethod

from jsp.jsp import Jsp

from schedulers.cpsat_scheduler import CpsatScheduler


class JspSource(ABC):

    """
    Abstract base class for a JSP source

    The method next needs to return the next JSP
    """

    def __init__(self, job_count, machine_count):
        self.job_count = job_count
        self.machine_count = machine_count

    def next(self) -> Jsp:

        jsp = self._next()

        if not self.job_count:
            raise(Exception('job_count was not initialized.'))
        if not self.machine_count:
            raise(Exception('machine_count was not initialized.'))
        if not jsp.job_count == self.job_count:
            raise(Exception('Generated JSP by JspSource does not have correct job count'))
        if not jsp.machine_count == self.machine_count:
            raise(Exception('Generated JSP by JspSource does not have correct machine count'))

        """
            # Do not compute optimal makespan by default
            if jsp.makespan_optimal is None:
                # Instead use CpsatSolver to get the optimal makespan
                jsp.makespan_optimal = CpsatScheduler(jsp).create_schedule().makespan
        """

        return jsp

    @abstractmethod
    def _next(self) -> Jsp:
        pass
