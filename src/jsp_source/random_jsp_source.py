"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from jsp_source.jsp_source import JspSource
from jsp.jsp_generator import JspGenerator


class RandomJspSource(JspSource):

    def _next(self):

        return JspGenerator().generate(self.job_count, self.machine_count)
