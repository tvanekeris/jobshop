"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from jsp.jsp import Jsp
from jsp_source.jsp_source import JspSource


class SingleJspSource(JspSource):

    def __init__(self, jsp: Jsp):
        # Deep copies are not required any more, because the JSP now only describes the problem, not the schedule
        self.jsp = jsp
        super().__init__(jsp.job_count, jsp.machine_count)

    def _next(self):
        return self.jsp
