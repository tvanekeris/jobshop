tqdm
numpy==1.19.3
gym
-f https://download.pytorch.org/whl/torch_stable.html torch==1.7.0
-f https://download.pytorch.org/whl/torch_stable.html torchvision==0.8.1
-f https://download.pytorch.org/whl/torch_stable.html torchaudio==0.7.0
tensorboard
stable_baselines3
ortools
xlsxwriter

umap-learn
playsound