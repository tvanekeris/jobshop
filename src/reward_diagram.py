"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import os
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

from drl.jsp_reward import JspReward


def draw_reward(x_min=0, x_max=200, show_window=False):
    x_arr = np.linspace(x_min, x_max, 200)
    y_arr = [JspReward.get_reward(1 + x / 100, 1) for x in x_arr]
    color = 'b'
    linewidth = 3
    label = 'Reward'

    width_in_inches = 20
    height_in_inches = 3
    dots_per_inch = 300

    plt.rcParams.update({'font.size': 20})

    plt.figure(figsize=(width_in_inches, height_in_inches), dpi=dots_per_inch)

    plt.plot(x_arr, y_arr, color=color, linewidth=linewidth, label=label)

    plt.xlabel(r'$m_a / m_{lb} - 1$ (in %)')
    plt.ylabel(r'Reward $r$')

    plt.xlim(left=0, right=200)
    plt.ylim(bottom=0)

    plt.grid(True)

    os.makedirs('../diagrams', exist_ok=True)
    filepath_out = Path('../diagrams') / f'reward.png'
    plt.savefig(filepath_out, bbox_inches='tight')
    filepath_out_svg = Path('../diagrams') / f'reward.svg'
    plt.savefig(filepath_out_svg, format='svg', dpi=300, bbox_inches='tight')

    if show_window:
        plt.show()


if __name__ == '__main__':
    draw_reward()
