import collections

# This source code file is based on Google's code from
# https://developers.google.com/optimization/scheduling/job_shop
# and adapted to fill the framework's object model

# CP-SAT is abbr. for Constraint Programming - Satisfiability

# Explanation of CP-SAT solver:
# https://www.cis.upenn.edu/~cis189/files/Lecture10.pdf

# Import Python wrapper for or-tools CP-SAT solver.
from ortools.sat.python import cp_model

from schedulers.scheduler import Scheduler


class CpsatScheduler(Scheduler):

    """Scheduler implemented via CP-SAT solver from Google OR tools"""

    @property
    def name(self):
        return 'CP-SAT'

    def _create_schedule(self):

        # TODO: Take preparations out here into the preparation method

        # Create the model.
        model = cp_model.CpModel()

        all_machines = range(self.schedule.machine_count)

        # Named tuple to store information about created variables.
        task_type = collections.namedtuple('task_type', 'start end interval')
        # Named tuple to manipulate solution information.
        assigned_task_type = collections.namedtuple('assigned_task_type',
                                                    'start job index duration')

        # Creates job intervals and add to the corresponding machine lists.
        all_tasks = {}
        machine_to_intervals = collections.defaultdict(list)

        for job in self.schedule.jobs:
            for task in job.tasks:
                suffix = '_%i_%i' % (job.idx, task.idx)
                # start_var and end_var are defined as new integers that can take on values
                # between lower bound 0 and upper bound horizon
                start_var = model.NewIntVar(0, self.schedule.horizon, 'start' + suffix)
                end_var = model.NewIntVar(0, self.schedule.horizon, 'end' + suffix)
                # Patch: end_var could also be defined with upper bound horizon-duration
                #end_var = model.NewIntVar(0, horizon-duration, 'end' + suffix)
                # The interval_var variable makes sure that the start_var and end_var take on values
                # that are *duration* apart
                interval_var = model.NewIntervalVar(start_var, task.duration, end_var,
                                                    'interval' + suffix)
                # Put the variables into the task dictionary
                all_tasks[job.idx, task.idx] = task_type(
                    start=start_var, end=end_var, interval=interval_var)
                # Add the interval variable to a list of intervals belonging to each machine
                # This is used to add NoOverlap conditions (because one interval can only be
                # 'active' on a machine at a time)
                machine_to_intervals[task.machine.idx].append(interval_var)

        # Create and add disjunctive constraints.
        for machine in all_machines:
            model.AddNoOverlap(machine_to_intervals[machine])

        # Precedences inside a job.
        for job in self.schedule.jobs:
            for task_idx in range(len(job.tasks) - 1):
                # Adds bounded linear expressions to the model
                # This makes sure that the tasks are processed in the correct order
                model.Add(all_tasks[job.idx, task_idx + 1].start >= all_tasks[job.idx, task_idx].end)

        # Makespan objective
        obj_var = model.NewIntVar(0, self.schedule.horizon, 'makespan')
        # The objective is the maximum of each job's end time...
        model.AddMaxEquality(obj_var, [
            # len(job) contains the number of tasks in the job
            # len(job)-1 denominates the last task in the job
            all_tasks[job.idx, len(job.tasks) - 1].end for job in self.schedule.jobs
        ])
        # ...which needs to be minimized
        model.Minimize(obj_var)

        # Solve model with CP-SAT solver
        solver = cp_model.CpSolver()
        solution_printer = CpsatSolverSolutionPrinter()
        status = solver.SolveWithSolutionCallback(model, solution_printer)

        if status == cp_model.OPTIMAL:
            # Create one list of assigned tasks per machine
            assigned_jobs = collections.defaultdict(list)
            for job in self.schedule.jobs:
                for task in job.tasks:
                    assigned_jobs[task.machine.idx].append(
                        assigned_task_type(
                            # Read out task start time from solver
                            start=solver.Value(all_tasks[job.idx, task.idx].start),
                            job=job.idx,
                            index=task.idx,
                            duration=task.duration))

            for machine_idx, machine in enumerate(self.schedule.machines):
                assigned_jobs[machine_idx].sort()
                for assigned_job in assigned_jobs[machine_idx]:
                    job = self.schedule.jobs[assigned_job.job]
                    task = job.tasks[assigned_job.index]
                    task.schedule_manual(assigned_job.start)

        elif status == cp_model.FEASIBLE:
            raise Exception('Problem was solved, but the optimal solution could not be found.')

        elif status == cp_model.INFEASIBLE:
            raise Exception('The CP model built from the JSP is infeasible to solve by CP-SAT.')


class CpsatSolverSolutionPrinter(cp_model.CpSolverSolutionCallback):
    """Print intermediate solutions."""

    def __init__(self):
        cp_model.CpSolverSolutionCallback.__init__(self)
        self.solution_count = 0

    def on_solution_callback(self):
        self.solution_count += 1

