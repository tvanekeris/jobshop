"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import logging
import os.path
from pathlib import Path
from typing import Optional, Type, List

from drl.jsp_feature import JspFeature
from jsp.jsp import Jsp
from jsp_source.single_jsp_source import SingleJspSource

from schedulers.scheduler import Scheduler

from drl.jsp_env import JspEnv
from drl.agent_file import AgentFile

# Switch: Use local files or site-package repository
# from drl.ppo.ppo import PPO
from stable_baselines3.ppo.ppo import PPO


class DrlScheduler(Scheduler):

    """
        Scheduler that uses a RL model

        Takes as input a model which has already been trained
    """

    def __init__(self, jsp: Optional[Jsp] = None, features: List[Type[JspFeature]] = None, agent_filepath: Path = None):

        super().__init__(jsp)

        if not features:
            raise Exception('No features given to DRL agent')

        self.features = features
        self.agent_filepath = agent_filepath

    @property
    def name(self):
        return 'DRL (ours)'

    # TODO: Pull loading of agent into __init__
    def _prepare_schedule(self):
        #filepath = AgentFile.agent_filepath(self.schedule.job_count, self.schedule.machine_count)

        logging.debug(f'Opening agent file: {self.agent_filepath}')

        if not os.path.exists(self.agent_filepath):
            raise Exception('There is no trained agent for the given JSP size. You need to train an agent first.')

        self.agent = PPO.load(self.agent_filepath)
        logging.debug(f'Action space data type: {self.agent.action_space}')
        logging.debug(f'Observation space data type: {self.agent.observation_space}')

    def _create_schedule(self):

        # TODO: Take environment loading etc. out of run time measurement

        env = JspEnv(SingleJspSource(self.jsp), self.features, loop_exception=True)

        obs = env.reset()
        done = False
        while not done:
            # Flag deterministic=True does not sample from the output layer distribution but chooses the most probable action
            action, _states = self.agent.predict(obs, deterministic=True)
            logging.debug(f'Agt: Action: {action}')
            obs, reward, done, info = env.step(action)

        self.schedule = env.last_schedule
