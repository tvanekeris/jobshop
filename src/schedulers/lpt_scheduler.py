"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import numpy as np

from schedulers.scheduler import Scheduler


class LptScheduler(Scheduler):

    """Scheduler implementing longest processing time task heuristic"""

    @property
    def name(self):
        return 'LPT'

    def _create_schedule(self):

        while True:
            durations = self.observer.duration_next_task_per_job
            if not durations.any():
                break
            job_idx = np.argmax(durations)
            job = self.schedule.jobs[job_idx]
            job.next_task_to_schedule.schedule_auto()

