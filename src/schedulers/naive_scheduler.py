"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from schedulers.scheduler import Scheduler
from jsp.jsp import Jsp


class NaiveScheduler(Scheduler):

    """Scheduler implementing a naive strategy that just schedules every task one by one"""

    @property
    def name(self):
        return 'Naive'

    def _create_schedule(self):

        for job in self.schedule.jobs:
            for task in job.tasks:
                task.schedule_auto()
