"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import random
import math

from jsp_source.single_jsp_source import SingleJspSource

from schedulers.scheduler import Scheduler
from jsp.schedule import Schedule


class RandomMultiScheduler(Scheduler):

    """Scheduler implementing a random strategy that runs multiple random tries and takes the best one"""

    RUN_COUNT = 20

    @property
    def name(self):
        return 'MultiRandom'

    def _create_schedule(self):

        self.jsp_source = SingleJspSource(self.jsp)

        minimum_makespan = math.inf
        best_schedule = None

        for _ in range(self.RUN_COUNT):

            jsp = self.jsp_source.next()
            schedule = Schedule(jsp)

            while not schedule.scheduled:
                job = random.choice(list(filter(lambda j: not j.scheduled, schedule.jobs)))
                job.next_task_to_schedule.schedule_auto()

            if schedule.makespan < minimum_makespan:
                minimum_makespan = schedule.makespan
                best_schedule = schedule

        self.schedule = best_schedule
