"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import random

from schedulers.scheduler import Scheduler
from jsp.jsp import Jsp


class RandomScheduler(Scheduler):

    """Scheduler implementing a random strategy"""

    @property
    def name(self):
        return 'Random'

    def _create_schedule(self):

        while not self.schedule.scheduled:
            job = random.choice(list(filter(lambda job: not job.scheduled, self.schedule.jobs)))
            job.next_task_to_schedule.schedule_auto()

