"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from abc import ABC, abstractmethod
from typing import Optional
import time

from jsp.jsp import Jsp
from jsp.schedule import Schedule
from jsp.observer import Observer


class Scheduler(ABC):

    """Scheduler Solver base class"""

    def __init__(self, jsp: Optional[Jsp] = None):

        self._jsp = None
        self.schedule = None
        self.observer = None
        self.solved = None

        if jsp is not None:
            self.set_jsp(jsp)

    @property
    def jsp(self):
        return self._jsp

    def set_jsp(self, jsp: Jsp):
        self._jsp = jsp

        # Reset schedule and observer as well
        self.schedule = Schedule(self.jsp)
        self.observer = Observer(self.schedule)
        self.solved = False

        return self

    def create_schedule(self):

        self._prepare_schedule()

        start = time.perf_counter()
        self._create_schedule()
        end = time.perf_counter()

        if self.name:
            self.schedule.name = self.name
        else:
            self.schedule.name = self.__class__.__name__

        self.schedule.calc_time = end-start
        return self.schedule

    @property
    @abstractmethod
    def name(self):
        pass

    def _prepare_schedule(self):
        pass

    @abstractmethod
    def _create_schedule(self):
        pass
