"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import numpy as np
import numpy.ma as ma

from schedulers.scheduler import Scheduler


class SptScheduler(Scheduler):

    """Scheduler implementing shortest processing time task heuristic"""

    @property
    def name(self):
        return 'SPT'

    def _create_schedule(self):

        while True:
            durations = self.observer.duration_next_task_per_job
            if not durations.any():
                break
            # Create mask so that completely scheduled task are not included
            job_idx = np.argmin(ma.masked_equal(durations, 0))
            job = self.schedule.jobs[job_idx]
            job.next_task_to_schedule.schedule_auto()
