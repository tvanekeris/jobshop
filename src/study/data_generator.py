"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import os
import os.path

import numpy as np

from multiprocessing import get_context, freeze_support, log_to_stderr
import logging

from tqdm import tqdm

from jsp.jsp import Jsp
from jsp.jsp_files import JspFiles
from jsp.jsp_generator import JspGenerator
from schedulers.cpsat_scheduler import CpsatScheduler


class DataGenerator:

    processes = 8

    def __init__(self, job_count, machine_count, jsp_count):
        self.job_count = job_count
        self.machine_count = machine_count
        self.jsp_count = jsp_count
        self.created_count = 0
        self.skipped_count = 0

    def generate_jsps(self):

        data_directory = JspFiles.data_directory(self.job_count, self.machine_count)

        os.makedirs(data_directory, exist_ok=True)

        logging.info('Generating JSPs and writing to disk:')
        jsp_generator = JspGenerator(np.random.default_rng(0))
        for jsp_idx in tqdm(range(self.jsp_count)):
            filepath = JspFiles.data_filepath(self.job_count, self.machine_count, jsp_idx)
            if os.path.exists(filepath):
                logging.debug(f'Data file {filepath} already exists. Data would be overwritten. Skipping...')
                self.skipped_count += 1
                continue
            else:
                self.created_count += 1
            jsp = jsp_generator.generate(self.job_count, self.machine_count)
            jsp.save_to_file(filepath)

    def calculate_makespan(self, jsp_idx):
        filepath = JspFiles.data_filepath(self.job_count, self.machine_count, jsp_idx)

        jsp = Jsp()
        jsp.load_from_file(filepath)

        if not jsp.makespan_optimal:
            makespan = CpsatScheduler(jsp).create_schedule().makespan
            jsp.makespan_optimal = makespan
            jsp.save_to_file(filepath)

    def calculate_makespans(self):
        logging.info('Calculating optimal makespans (in parallel):')

        with get_context('spawn').Pool(processes=self.processes) as pool:
            for _ in tqdm(pool.imap_unordered(self.calculate_makespan, range(self.jsp_count)), total=self.jsp_count):
                pass

            pool.close()
            pool.join()

    def __str__(self):
        text = f'Data generator for {self.job_count}x{self.machine_count} JSPs'
        if self.created_count == 0 and self.skipped_count == 0:
            text += f'\nDid not create any data so far.'
        else:
            text += f'\nCreated {self.created_count} JSPs and skipped {self.skipped_count} JSPs'

        return text


if __name__ == '__main__':

    data_generator = DataGenerator(12, 12, 1000)
    data_generator.generate_jsps()
    data_generator.calculate_makespans()
