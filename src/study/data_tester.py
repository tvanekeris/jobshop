"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from tqdm import tqdm

from jsp.jsp import Jsp
from jsp.jsp_files import JspFiles


class DataTester:

    def __init__(self, job_count, machine_count, jsp_count):
        self.job_count = job_count
        self.machine_count = machine_count
        self.jsp_count = jsp_count

    def test(self):

        print('Testing JSPs:')

        for jsp_idx in tqdm(range(self.jsp_count)):
            filepath = JspFiles.data_filepath(self.job_count, self.machine_count, jsp_idx)

            # TODO: Check that valid

            # TODO: Check that makespan is present
            # This check is not needed, because this is ensured by the DataGenerator
            #jsp = Jsp()
            #jsp.load_from_file(filepath)
            #if not jsp.makespan_optimal:
            #    raise Exception(f'JSP {jsp_idx} / {jsp.name} does not contain an optimal makespan')

            # TODO: Check uniqueness


if __name__ == '__main__':

    data_tester = DataTester(12, 12, 1000)
    data_tester.test()
