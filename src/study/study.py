"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import os
import logging
import datetime
from pathlib import Path
from typing import Type, List, Optional

from drl.jsp_feature import JspFeature
from jsp.jsp_files import JspFiles
from jsp.schedule_distance import ScheduleDistancePermutations, ScheduleDistanceTaskStart
from study.data_generator import DataGenerator
from study.data_tester import DataTester
from study.trainer import Trainer
from study.tester import Tester


class Study:

    def __init__(self, job_count=12, machine_count=12, data_gen=True, training=True, training_jsp_start_idx=0, training_jsp_count=10_000, training_timesteps=250_000, features: List[Type[JspFeature]] = None, testing=True, testing_jsp_start_idx=None, testing_jsp_count=1000):

        self.timestamp = datetime.datetime.now()

        self.job_count = job_count
        self.machine_count = machine_count

        self.data_gen = data_gen

        self.training = training
        self.training_jsp_start_idx = training_jsp_start_idx
        self.training_jsp_count = training_jsp_count
        self.training_timesteps = training_timesteps
        if not features:
            raise Exception('No features given.')
        self.features = features

        self.testing = testing
        self.testing_jsp_start_idx = testing_jsp_start_idx
        self.testing_jsp_count = testing_jsp_count
        self.jsps_testing = []

    @property
    def id(self):
        return f'{self.timestamp.strftime("%Y-%m-%d_%H-%M-%S")}_{self.job_count}x{self.machine_count}'

    @property
    def name(self):
        return f'study_{self.id}'

    @property
    def directory(self):
        return Path('..') / 'studies' / self.name

    def run(self):

        # TODO: This is not 100% correct (e.g., if the testing_jsp_start_idx is bigger), it needs to handle more cases
        jsp_count = self.training_jsp_count + self.testing_jsp_count

        if self.data_gen:

            data_generator = DataGenerator(self.job_count, self.machine_count, jsp_count)
            data_generator.generate_jsps()
            data_generator.calculate_makespans()

            data_generator_log = str(data_generator)

            data_generation_filepath = self.directory / f'data_generation_{self.id}.txt'
            os.makedirs(self.directory, exist_ok=True)
            with open(data_generation_filepath, 'w') as makespan_diffs_file:
                print(data_generator_log, file=makespan_diffs_file)

            logging.info(data_generator_log)

            data_tester = DataTester(self.job_count, self.machine_count, jsp_count)
            data_tester.test()

        if self.training:
            trainer = Trainer(self, self.training_timesteps, 0, self.training_jsp_count)
            trainer.train_agent(overwrite=True)

        if self.testing:
            if self.testing_jsp_start_idx:
                testing_jsp_start_idx = self.testing_jsp_start_idx
            else:
                testing_jsp_start_idx = self.training_jsp_count

            self.jsps_testing = JspFiles.load_jsp_list(self.job_count, self.machine_count, testing_jsp_start_idx, self.testing_jsp_count)

            # For test with Vladimirs or Tilos data
            #self.jsps_testing = JspFiles.load_jsp_list_from_dir(JspFiles.data_directory_raw() / 'data-vladimir', includes_header=False, job_count=6, machine_count=6)
            #self.jsps_testing = JspFiles.load_jsp_list_from_dir(JspFiles.data_directory_raw() / 'data-tilo')

            tester = Tester(self)
            tester.generate_schedules()
            tester.create_statistics()
            tester.print_report()
            tester.backup_files()

            tester.write_distance_excel(ScheduleDistancePermutations)

            for distance_metric in tester.distance_metrics:
                tester.plot_distance_distribution(distance_metric)
