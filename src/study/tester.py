"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import logging
import datetime

import os.path
from pathlib import Path

import numpy as np

import itertools
from functools import reduce

import matplotlib.pyplot as plt

from texttable import Texttable
import xlsxwriter
from tqdm import tqdm

from jsp.schedule_distance import ScheduleDistanceMakespan, ScheduleDistanceTaskCount, \
    ScheduleDistanceTaskStart, ScheduleDistancePermutations
from schedulers.naive_scheduler import NaiveScheduler
from schedulers.lrd_scheduler import LrdScheduler
from schedulers.srd_scheduler import SrdScheduler
from schedulers.random_multi_scheduler import RandomMultiScheduler
from schedulers.random_scheduler import RandomScheduler
from schedulers.drl_scheduler import DrlScheduler
from schedulers.cpsat_scheduler import CpsatScheduler
from schedulers.spt_scheduler import SptScheduler
from schedulers.lpt_scheduler import LptScheduler

np.set_printoptions(edgeitems=30, linewidth=200)
# np.warnings.filterwarnings('error', category=np.VisibleDeprecationWarning)


class Tester:

    # TODO: Tester should not load data by itself but rather be passed a Jsp Generator

    def __init__(self, study):#, jsp_start_idx, jsp_count):

        self.study = study
        self.schedules = []
        self.makespan_optimal_cum = None

        #self.jsp_start_idx = jsp_start_idx
        self.jsp_count = len(self.study.jsps_testing)

        # TODO: For agent, choose between study filepath and general filepath

        #self.schedulers_c = [CpsatScheduler, RlPpoScheduler, GrdScheduler, RandomMultiScheduler, RandomScheduler, GptScheduler, SptScheduler, NaiveScheduler, SrdScheduler]
        self.schedulers_c = [CpsatScheduler, DrlScheduler, LrdScheduler, RandomScheduler, LptScheduler, SptScheduler, SrdScheduler, NaiveScheduler]
        self.schedulers = []
        for scheduler_c in self.schedulers_c:
            if scheduler_c == DrlScheduler:
                # Give extra options to the DrlScheduler (features and the agent filepath)
                self.schedulers.append(scheduler_c(None, self.study.features, self.study.directory / 'drl_agent.zip'))
            else:
                self.schedulers.append(scheduler_c())
        #self.schedulers = [scheduler_c() for scheduler_c in self.schedulers_c]
        self.schedulers_names = [scheduler.name for scheduler in self.schedulers]

        # Combinations for distance metrics
        self.combinations_c = [DrlScheduler, LrdScheduler, LptScheduler, CpsatScheduler, SrdScheduler, SptScheduler]
        self.combinations = list(itertools.combinations(self.combinations_c, 2))
        for c in list(self.combinations):
            if DrlScheduler not in c:
                self.combinations.remove(c)
        self.combinations_count = len(self.combinations)
        self.combinations_idxs = [(self.schedulers_c.index(scheduler1), self.schedulers_c.index(scheduler2)) for (scheduler1, scheduler2) in self.combinations]
        self.combinations_names = [f'{self.schedulers_names[scheduler1_idx]} vs. {self.schedulers_names[scheduler2_idx]}' for (scheduler1_idx, scheduler2_idx) in self.combinations_idxs]

        # Distance metrics
        self.distance_metrics = [ScheduleDistanceMakespan, ScheduleDistanceTaskCount, ScheduleDistanceTaskStart, ScheduleDistancePermutations]

        # Results
        # TODO: Make matrix, save every occurrence to be able to calculate stdev
        shape = (len(self.schedulers), self.jsp_count)
        self.makespans_achieved = np.zeros(shape, dtype=int)
        self.makespans_delta_optimal = np.zeros(shape, dtype=int)
        self.runtimes = np.zeros(shape, dtype=float)

        # TODO: Check if I can make this float
        self.distances = np.empty((self.jsp_count, self.combinations_count, len(self.distance_metrics)), dtype=int)

    def generate_schedules(self):
        #jsps = JspFiles.load_jsp_list(self.study.job_count, self.study.machine_count, self.jsp_start_idx, self.jsp_count)
        jsps = self.study.jsps_testing

        self.makespan_optimal_cum = reduce(lambda makespan_optimal_cum, jsp: makespan_optimal_cum + jsp.makespan_optimal, jsps, 0)

        if self.jsp_count != len(jsps):
            raise Exception('Could not load as many JSPs as requested for testing!')

        logging.info('Running schedulers...')

        self.schedules = []

        for jsp_idx, jsp in enumerate(tqdm(jsps)):

            if jsp.makespan_optimal is None:
                raise Exception(f'Trying to load a JSP without optimal makespan {jsp.name}')

            self.schedules.append([scheduler.set_jsp(jsp).create_schedule() for scheduler in self.schedulers])

    def create_statistics(self):

        logging.info('Acquiring statistics...')

        for jsp_idx in tqdm(range(self.jsp_count)):

            schedules = self.schedules[jsp_idx]

            self.makespans_achieved[:, jsp_idx] = np.array([schedule.makespan for schedule in schedules])
            self.makespans_delta_optimal[:, jsp_idx] = np.array([schedule.makespan - schedule.makespan_optimal for schedule in schedules])
            self.runtimes[:, jsp_idx] = np.array([schedule.calc_time for schedule in schedules])

            for distance_metric_idx, distance_metric in enumerate(self.distance_metrics):
                # TODO: Check if I can make this float (see above)
                self.distances[jsp_idx, :, distance_metric_idx] = np.array([distance_metric.measure(schedules[idx1], schedules[idx2]) for (idx1, idx2) in self.combinations_idxs], dtype=int)

        os.makedirs(self.study.directory, exist_ok=True)

        for distance_metric_idx, distance_metric in enumerate(self.distance_metrics):

            distance_measure_filepath = self.study.directory / f'distance_{distance_metric.__name__}_{self.study.id}.txt'
            with open(distance_measure_filepath, 'w') as distance_measure_file:
                self.write_distance_textfile(self.distances[:, :, distance_metric_idx], distance_measure_file)

        makespans_spt_filepath = self.study.directory / f'makespans_spt_{self.study.id}.txt'
        with open(makespans_spt_filepath, 'w') as makespans_spt_file:
            for jsp_idx in tqdm(range(self.jsp_count)):
                schedules = self.schedules[jsp_idx]
                schedule_spt = schedules[self.schedulers_c.index(SptScheduler)]
                print(f'{self.study.jsps_testing[jsp_idx]}: {schedule_spt.makespan}', file=makespans_spt_file)

    def print_report(self):

        result_text = f'Sum of optimal makespans: {self.makespan_optimal_cum}\n\n'

        texttable = Texttable(200)
        texttable.set_deco(Texttable.HEADER)
        texttable.set_precision(1)

        texttable.header([
            '\nScheduler',
            'Makespan\ncum',
            '\nmean',
            '\nstdev',
            '\nmin',
            '\nmedian',
            '\nmax',
            'Err./prblm.\ncum',
            '\nmean',
            '\nstdev',
            '\nmin',
            '\nmedian',
            '\nmax',
            'Rel. Err.\nmean',
            'Time/prblm. (ms)\ncum',
            '\nmean',
            '\nstdev',
            '\nmin',
            '\nmedian',
            '\nmax'
        ])

        texttable.set_cols_align(['r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r'])
        texttable.set_header_align(['r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r'])

        for scheduler_idx, scheduler in enumerate(self.schedulers):
            texttable.add_row([
                scheduler.name,
                np.sum(self.makespans_achieved[scheduler_idx, :]),
                np.mean(self.makespans_achieved[scheduler_idx, :]),
                np.std(self.makespans_achieved[scheduler_idx, :]),
                np.min(self.makespans_achieved[scheduler_idx, :]),
                np.median(self.makespans_achieved[scheduler_idx, :]),
                np.max(self.makespans_achieved[scheduler_idx, :]),
                np.sum(self.makespans_delta_optimal[scheduler_idx, :]),
                np.mean(self.makespans_delta_optimal[scheduler_idx, :]),
                np.std(self.makespans_delta_optimal[scheduler_idx, :]),
                np.min(self.makespans_delta_optimal[scheduler_idx, :]),
                np.median(self.makespans_delta_optimal[scheduler_idx, :]),
                np.max(self.makespans_delta_optimal[scheduler_idx, :]),
                np.sum(self.makespans_delta_optimal[scheduler_idx, :]) / self.makespan_optimal_cum * 100,
                int(np.sum(self.runtimes[scheduler_idx, :]) * 1000.0),
                np.mean(self.runtimes[scheduler_idx, :]) * 1000.0,
                np.std(self.runtimes[scheduler_idx, :]) * 1000.0,
                np.min(self.runtimes[scheduler_idx, :]) * 1000.0,
                np.median(self.runtimes[scheduler_idx, :]) * 1000.0,
                np.max(self.runtimes[scheduler_idx, :]) * 1000.0
            ])

        result_text += texttable.draw()

        print(result_text)

        # Get agent from DRL scheduler
        agent = self.schedulers[self.schedulers_c.index(DrlScheduler)].agent

        study_filepath = self.study.directory / f'report_{self.study.id}.txt'
        os.makedirs(self.study.directory, exist_ok=True)
        with open(study_filepath, 'w') as study_file:
            print(f'-----------------------------------------------------------------------------------------------------------------', file=study_file)
            print(f'Reinforcement Learning study report from {self.study.timestamp:%Y-%m-%d %H:%M:%S}', file=study_file)
            print(f'-----------------------------------------------------------------------------------------------------------------', file=study_file)
            print(f'', file=study_file)
            print(f'Reinforcement Learning Agent:', file=study_file)
            print(f'-----------------------------', file=study_file)
            print(f'Agent was trained on:    {datetime.datetime.fromtimestamp(agent.start_time):%Y-%m-%d %H:%M:%S}', file=study_file)
            print(f'Observation space:       {agent.observation_space}', file=study_file)
            print(f'Action space:            {agent.action_space}', file=study_file)
            print(f'Network architecture:    {agent.policy.net_arch}', file=study_file)
            print(f'# of training timesteps: {agent.num_timesteps}', file=study_file)
            print(f'', file=study_file)
            print(f'Test set:', file=study_file)
            print(f'---------', file=study_file)
            print(f'# of problems:    {self.jsp_count}', file=study_file)
            print(f'Size of problems: {self.study.job_count} jobs / {self.study.machine_count} machines', file=study_file)
            print(f'', file=study_file)
            print(f'Results:', file=study_file)
            print(f'--------', file=study_file)
            print(result_text, file=study_file)
            print(f'', file=study_file)
            print(f'-----------------------------------------------------------------------------------------------------------------\n', file=study_file)
            print(f'Comments:\n\n\n', file=study_file)
            print(f'-----------------------------------------------------------------------------------------------------------------\n', file=study_file)

    @classmethod
    def prepare_counts(cls, distances, combi_idx):

        return np.unique(distances[:, combi_idx], return_counts=True)

    @classmethod
    def complete_histogram(cls, distance_values, distance_counts, target_value_range):

        """
        Complete a histogram
        :param distance_values:
        :param distance_counts:
        :param target_value_range:
        :return:
        """

        x_arr = []
        y_arr = []

        for value in target_value_range:
            if value in distance_values:
                # TODO: Wow, this is ugly. Make it nicer... Directly construct an np-array or so...
                diff = distance_counts[np.where(distance_values == value)][0]
                x_arr.append(value)
                y_arr.append(diff)
            else:
                x_arr.append(value)
                y_arr.append(0)

        return x_arr, y_arr

    def prepare_distributions(self, distances, symmetric=False, x_min=0, x_max=100):

        # Make sure that values from x_min to x_max are always "filled", so that in the graph there
        # are no "gaps"
        minimum_value = min(x_min, np.min(distances))
        maximum_value = max(x_max, np.max(distances))
        symmetric_maximum_value = max(abs(minimum_value), abs(maximum_value))

        if symmetric:
            value_range = range(-symmetric_maximum_value, symmetric_maximum_value+1)
        else:
            value_range = range(minimum_value, maximum_value+1)

        distributions = []
        means = []
        stds = []

        # Write counts for every combination
        for combi_idx in range(self.combinations_count):

            (diff_values, diff_counts) = Tester.prepare_counts(distances, combi_idx)

            x_arr, y_arr = Tester.complete_histogram(diff_values, diff_counts, value_range)

            distributions.append((x_arr, y_arr))

            mean = np.mean(distances[:, combi_idx])
            means.append(mean)
            std = np.std(distances[:, combi_idx])
            stds.append(std)

        return distributions, means, stds

    def write_distance_textfile(self, distances, file):

        print(f'\nMinimum value: {np.min(distances)}', file=file)
        print(f'\nMaximum value: {np.max(distances)}', file=file)

        print(f'-------------------------------------------------------------------------------------------------------', file=file)
        print(f'Distribution differences:', file=file)

        for combi_idx in range(self.combinations_count):
            print(self.combinations_names[combi_idx], file=file)

            (diff_values, diff_counts) = Tester.prepare_counts(distances, combi_idx)

            print(diff_values, file=file)
            print(diff_counts, file=file)

    def write_distance_excel(self, distance_metric, symmetric=False):

        # TODO: Write one excel tab per distance measure

        filepath = self.study.directory / f'distances_{self.study.id}.xlsx'

        workbook = xlsxwriter.Workbook(filepath)
        worksheet = workbook.add_worksheet('Distribution')

        worksheet.write(0, 0, 'Delta')

        distributions, means, stds = self.prepare_distributions(self.distances[:, :, self.distance_metrics.index(distance_metric)], symmetric)

        # Loop through the combinations
        for combination_idx in range(self.combinations_count):

            # Write table headers
            col = 1 + combination_idx * 2
            worksheet.write(0, col, self.combinations_names[combination_idx])

            x_arr, y_arr = distributions[combination_idx]

            for idx, x_value in enumerate(x_arr):

                row = idx + 1

                # Write values only once
                if combination_idx == 0:
                    worksheet.write(row, 0, x_value)

                # Write counts for every combination
                worksheet.write(row, col, y_arr[idx])

                worksheet.write(0, col+1, means[combination_idx])
                worksheet.write(1, col + 1, stds[combination_idx])

        workbook.close()

    def plot_distance_distribution(self, distance_metric, show_window=False):

        x_min = 0
        x_max = 100

        distributions, _, _ = self.prepare_distributions(self.distances[:, :, self.distance_metrics.index(distance_metric)], x_min=x_min, x_max=x_max)

        plot_values = []

        # Write counts for every combination
        for combination_idx, (scheduler1, scheduler2) in enumerate(self.combinations):

            highlight_class = DrlScheduler
            if scheduler1 == highlight_class or scheduler2 == highlight_class:
                # Use default color
                color = None
                linewidth = 3
            else:
                color = 'k'
                linewidth = 1

            x_arr, y_arr = distributions[combination_idx]

            plot_values.append((x_arr, y_arr, color, linewidth, self.combinations_names[combination_idx]))

        width_in_inches = 20
        height_in_inches = 5
        dots_per_inch = 300

        plt.rcParams.update({'font.size': 20})

        plt.figure(figsize=(width_in_inches, height_in_inches), dpi=dots_per_inch)

        for x_arr, y_arr, color, linewidth, label in plot_values:
            plt.plot(x_arr, y_arr, color=color, linewidth=linewidth, label=label)
        #plt.title(f'Scheduler distances (metric {distance_metric.__name__})')

        plt.xlabel('Schedule distance (CATD)')
        plt.ylabel('Count')

        plt.xlim(left=x_min, right=x_max)
        plt.ylim(bottom=0)

        plt.grid(True)

        filepath_out = self.study.directory / f'{distance_metric.__name__}.png'
        plt.savefig(filepath_out, bbox_inches='tight')
        filepath_out_svg = self.study.directory / f'{distance_metric.__name__}.svg'
        plt.savefig(filepath_out_svg, format='svg', dpi=300, bbox_inches='tight')

        plt.legend()

        filepath_out = self.study.directory / f'{distance_metric.__name__}_legend.png'
        plt.savefig(filepath_out, bbox_inches='tight')
        filepath_out_svg = self.study.directory / f'{distance_metric.__name__}_legend.svg'
        plt.savefig(filepath_out_svg, format='svg', dpi=300, bbox_inches='tight')

        if show_window:
            plt.show()

    def copy_file(self, filepath_in):
        # TODO: Maybe use OS function
        # TODO: Save whole sources dir to study
        filepath_out = self.study.directory / 'src' / filepath_in
        os.makedirs(filepath_out.parent, exist_ok=True)
        with open(filepath_out, 'w') as file_out:
            with open(filepath_in, 'r') as file_in:
                print(file_in.read(), file=file_out)

    def backup_files(self):
        files_to_backup = [Path('drl') / f'jsp_reward.py',
                           Path('drl') / f'jsp_env.py']

        for filepath in files_to_backup:
            self.copy_file(filepath)

    def plot_umap(self):
        """
        matrix = self.distance_matrix_from_vector(means, 6)
        mapper = umap.UMAP(n_neighbors=2, min_dist=0.0, metric='precomputed').fit(matrix)
        hover_data = pd.DataFrame({'index': np.arange(6), 'label': np.arange(6)})
        p = umap.plot.interactive(mapper, labels=np.arange(6), hover_data=hover_data)
        #umap.plot.plt.show()
        umap.plot.show(p)
        """


if __name__ == '__main__':

    raise Exception('This module cannot be run directly. Instantiate by passing a study object.')
