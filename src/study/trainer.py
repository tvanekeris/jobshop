"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import os
from pathlib import Path

# TODO: Implement vectorized environments
from stable_baselines3.common.env_util import make_vec_env

from stable_baselines3.ppo.policies import MlpPolicy
from stable_baselines3.ppo.ppo import PPO
from stable_baselines3.common.monitor import Monitor

from helpers.logging_config import logging_config

from jsp.jsp import Jsp
from jsp_source.cyclic_data_jsp_source import CyclicDataJspSource
from jsp_source.random_jsp_source import RandomJspSource
from jsp_source.single_jsp_source import SingleJspSource
from drl.jsp_env import JspEnv
from drl.agent_file import AgentFile


class Trainer:

    def __init__(self, study, training_timesteps, jsp_start_idx, jsp_count):

        self.study = study

        self.training_timesteps = training_timesteps

        self.jsp_start_idx = jsp_start_idx
        self.jsp_count = jsp_count

    def train_agent(self, overwrite=False):

        print('Training RL PPO solver')

        # TODO: Implement parallel processing by using vectorized environments
        # INFO: Parallel processing takes 60 seconds with 50.000 total timesteps
        # https://stable-baselines3.readthedocs.io/en/master/guide/vec_envs.html#dummyvecenv
        #env = make_vec_env(SchedulingEnv, n_envs=4)

        mode = 'data'

        if mode == 'data':
            jsp_source = CyclicDataJspSource(self.study.job_count, self.study.machine_count, self.jsp_start_idx, self.jsp_count)
        elif mode == 'random':
            # Train agent from random JSP source
            jsp_source = RandomJspSource(3, 3)
        else:
            # Train agent from single JSP
            filename = Path('..') / 'data' / 'tve' / 've_10_10_02.dat'
            jsp = Jsp()
            jsp.load_from_file(filename)
            jsp_source = SingleJspSource(jsp)

        env = JspEnv(jsp_source, self.study.features, loop_exception=False)

        env = Monitor(env, '../logs')

        agent = PPO(policy=MlpPolicy, env=env, gamma=0.99, verbose=1, tensorboard_log='../tensorboard/')
        agent.learn(total_timesteps=self.training_timesteps, tb_log_name=self.study.name)

        print(f'The training set contained {self.jsp_count} problems.')
        print(f'{env.env_resets_count} problems were used.')
        if self.jsp_count > env.env_resets_count:
            print(f'Thus not all training set problems have been used.')
        else:
            print(f'Thus some problems from the training set have been used more than once.')
        print(f'The last JSP used was {env.jsp.name}')

        agent_filepath_study = self.study.directory / 'drl_agent.zip'
        agent.save(agent_filepath_study)

        agent_filepath_general = AgentFile.agent_filepath(self.study.job_count, self.study.machine_count)
        if (not os.path.exists(agent_filepath_general)) or (os.path.exists(agent_filepath_general) and overwrite):
            agent.save(agent_filepath_general)

        print(f'The trained policy network size is {agent.policy.net_arch}')

        print('Training completed!')


if __name__ == '__main__':

    raise Exception('This module cannot be run directly. Instantiate by passing a study object.')

