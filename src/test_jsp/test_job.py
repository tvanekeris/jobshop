"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import unittest

import os

from jsp.jsp import Jsp
from jsp.schedule import Schedule


class JobTestCase(unittest.TestCase):

    def setUp(self) -> None:
        filepath = os.path.join('..', 'problems', '4_4_example.dat')

        with open(filepath, 'r') as file:
            self.or_format_string = file.read()

        self.jsp = Jsp()
        self.jsp.load_from_file(filepath)
        self.schedule = Schedule(self.jsp)

        self.job = self.schedule.jobs[0]

    def test_job_unscheduled_task_count(self):
        self.assertEqual(self.job.task_count_unscheduled, self.job.task_count)
        self.job.next_task_to_schedule.schedule_auto()
        task_count_unscheduled_new = self.job.task_count - 1
        self.assertEqual(self.job.task_count_unscheduled, task_count_unscheduled_new)

    def tearDown(self) -> None:
        pass


if __name__ == '__main__':

    unittest.main()
