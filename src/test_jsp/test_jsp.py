"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import unittest
import os
import re

from jsp.jsp import Jsp


class JspTestCase(unittest.TestCase):

    def setUp(self) -> None:
        filepath = os.path.join('..', 'problems', '4_4_example.dat')

        with open(filepath, 'r') as file:
            self.or_format_string = file.read()

        self.jsp = Jsp()
        self.jsp.load_from_file(filepath)

    def test_or_format_string_present(self):
        self.assertGreaterEqual(len(self.jsp.or_format_string), 3)

    @staticmethod
    def equalize_or_format(or_format_string):
        # Ignore more spaces in between numbers
        # (becuase when we output in OR format, we right-align numbers)
        or_format_string = re.sub(r' +', ' ', or_format_string)
        return or_format_string

    def test_or_format_string_equality(self):
        string1 = JspTestCase.equalize_or_format(self.jsp.or_format_string)
        string2 = JspTestCase.equalize_or_format(self.or_format_string)
        self.assertEqual(string1, string2)
        self.assertEqual(self.jsp.or_format_string, self.or_format_string)

    def tearDown(self) -> None:
        pass


if __name__ == '__main__':

    unittest.main()
