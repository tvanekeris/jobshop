"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

import unittest

import numpy as np

from jsp.jsp_generator import JspGenerator


class JspTestCase(unittest.TestCase):

    def setUp(self) -> None:

        self.gen1 = JspGenerator(np.random.default_rng(0))
        self.gen2 = JspGenerator(np.random.default_rng(0))
        self.gen3 = JspGenerator(np.random.default_rng(1))

    def test_deterministic_generation(self):

        for idx in range(10):
            jsp1 = self.gen1.generate(20, 20)
            jsp2 = self.gen2.generate(20, 20)
            jsp3 = self.gen3.generate(20, 20)

            self.assertEqual(jsp1, jsp2)
            self.assertNotEqual(jsp2, jsp3)

    def tearDown(self) -> None:
        pass


if __name__ == '__main__':

    unittest.main()
