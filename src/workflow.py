"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from playsound import playsound

from drl.bottleneck_feature import BottleneckFeature
from drl.rjd_feature import RjdFeature
from drl.rtc_feature import RtcFeature
from helpers.logging_config import logging_config

from study.study import Study


def main():
    studies = [

        # Run the studies required for the paper results

        # Study for the 8x8 result table
        Study(job_count=8, machine_count=8, features=[RjdFeature, BottleneckFeature, RtcFeature], training_timesteps=25000),

        # Study for the 12x12 result table
        #Study(job_count=12, machine_count=12, features=[RjdFeature, BottleneckFeature, RtcFeature]),

    ]

    for study in studies:
        try:
            study.run()
        except:
            playsound('failure.mp3')
            raise

        playsound('success.mp3')


if __name__ == '__main__':
    logging_config(description='Running complete study workflow')
    main()
