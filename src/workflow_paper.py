"""
Copyright (c) 2021 Tilo van Ekeris
Distributed under the MIT license, see the accompanying
file LICENSE or https://opensource.org/licenses/MIT
"""

from helpers.logging_config import logging_config

from drl.rjd_feature import RjdFeature
from drl.ntd_feature import NtdFeature
from drl.bottleneck_feature import BottleneckFeature
from drl.rtc_feature import RtcFeature

from study.study import Study


def main():

    # Run the code for all paper figures and results in order

    # Figure 1a, Figure 1b, Figure 3
    # This solves the example JSP with the CP-SAT solver and
    # prints a nicely formatted excel file
    # --> Run `python jsp_example_xlsx.py`

    # Figure 4
    # --> Run `python reward_diagram.py`

    # Experiment E1

    # Figure 5
    Study(job_count=12, machine_count=12, features=[RjdFeature]).run()
    # The resulting diagram can then be found in the "studies" directory in the according subdirectory
    # with the filename: ScheduleDistanceTaskStart_legend.svg (or .png)

    # Figure 6
    Study(job_count=12, machine_count=12, features=[NtdFeature]).run()
    # The resulting diagram can then be found in the "studies" directory in the according subdirectory
    # with the filename: ScheduleDistanceTaskStart_legend.svg (or .png)

    # Experiment E2

    # Study for the 8x8 result table (Table 1)
    Study(job_count=8, machine_count=8, features=[RjdFeature, BottleneckFeature, RtcFeature]).run()
    # The result table can then be found in the "studies" directory in the according subdirectory
    # with the filename: report_*.txt

    # Study for the 12x12 result table (Table 2)
    Study(job_count=12, machine_count=12, features=[RjdFeature, BottleneckFeature, RtcFeature]).run()
    # The result table can then be found in the "studies" directory in the according subdirectory
    # with the filename: report_*.txt


if __name__ == '__main__':
    logging_config(description='Running complete workflow for paper results and diagrams')
    main()
